package az.hotel.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    static SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

    public static String convertDateToString(Date value) throws ParseException {
        return formatter.format(value);

    }
}
