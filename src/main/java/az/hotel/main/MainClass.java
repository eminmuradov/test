package az.hotel.main;
import az.hotel.config.WebConfig;
import az.hotel.model.Customer;
import az.hotel.service.CustomerService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class MainClass {
    public static void main(String[] args) throws SQLException {
        ApplicationContext context= new AnnotationConfigApplicationContext(WebConfig.class);
    DataSource dataSource= (DataSource) context.getBean("dataSource");
    Connection connection=dataSource.getConnection();
    if (connection!=null){
        System.out.println("success");
    }

        CustomerService customerService= (CustomerService) context.getBean("customerService");

    List<Customer>customers=customerService.getCustomers();
    for (Customer customer:customers){
        System.out.println(customer);
    }

    }
}
