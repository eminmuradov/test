package az.hotel.dao;

import az.hotel.model.Customer;

import java.util.List;

public interface CustomerDao {
    List<Customer> getCustomers();
    List<Customer> getCustomersHibernate();
    void saveCustomers(Customer customer);
    Customer getCustomerHibernate();

    Object getNationality();
}
