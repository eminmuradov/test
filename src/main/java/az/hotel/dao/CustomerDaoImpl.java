package az.hotel.dao;

import az.hotel.model.Customer;
import az.hotel.model.Nationality;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Repository

public class CustomerDaoImpl extends AbstractDao implements CustomerDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Customer> getCustomers() {

        String sql = "select*from customers where active=1";
        Object obj[] = new Object[]{};
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<Customer>(Customer.class));
    }


    public List<Customer> getCustomersHibernate() {
        return session().createQuery("from Customer", Customer.class).list();
    }

    public void saveCustomers(Customer customer) {
        session().save(customer);
    }

    public Customer getCustomerHibernate() {
        return session().get(Customer.class, 6);
    }

    public Object getNationality() {
        return session().createQuery("from Nationality ", Nationality.class);
    }
}
