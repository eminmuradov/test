package az.hotel.controller;

import az.hotel.model.Customer;
import az.hotel.model.Nationality;
import az.hotel.service.CustomerService;
import az.hotel.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;


@RequestMapping(value = "/services")
@Controller
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "/sayHello", method = RequestMethod.GET)
    public String sayHello(ModelMap model) {
        model.addAttribute("message", "Hello world!");
        return "index";
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String getCustomersHibernate(ModelMap model) {
        try {
            model.addAttribute("customers", customerService.getCustomersHibernate());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "index";
    }

    @RequestMapping(value = "/customer", method = RequestMethod.GET)
    public String getCustomerHibernate() {
        ModelMap model = new ModelMap();
        model.addAttribute("customer", customerService.getCustomerHibernate());
        return "index";
    }

    @RequestMapping(value = "nationality", method = RequestMethod.GET)
    public String getNationality(ModelMap model) {
        model.addAttribute("nationality", customerService.getNationality());
        return "index";

    }

    @RequestMapping(value = "/saveCustomer", method = RequestMethod.POST)
    public String submit(Customer customer, ModelMap model) throws ParseException {


        System.out.println(model.addAttribute("name", customer.getName()));
        System.out.println(model.addAttribute("surname", customer.getSurname()));
        System.out.println(model.addAttribute("dob", DateUtil.convertDateToString(customer.getDob())));
        System.out.println(model.addAttribute("active", customer.getActive()));
        System.out.println(model.addAttribute("nationality_id",customer.getNationality().getId()));
        customerService.saveCustomer(customer);
        System.out.println("Success");
        return "index";
    }
}


