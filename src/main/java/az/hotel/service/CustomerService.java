package az.hotel.service;

import az.hotel.model.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> getCustomers();

    List<Customer> getCustomersHibernate();

    void saveCustomer(Customer customer);

    Customer getCustomerHibernate();

    Object getNationality();
}
