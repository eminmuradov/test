package az.hotel.service;

import az.hotel.dao.CustomerDao;
import az.hotel.model.Customer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {
    CustomerDao customerDao;

    public CustomerServiceImpl(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    public List<Customer> getCustomers() {
        return customerDao.getCustomers();
    }

    public List<Customer> getCustomersHibernate(){
        return customerDao.getCustomersHibernate();
    }

    public void saveCustomer(Customer customer) {
         customerDao.saveCustomers(customer);
    }
    public Customer getCustomerHibernate(){
        return customerDao.getCustomerHibernate();
    }

    public Object getNationality() {
        return customerDao.getNationality();
    }


}
