package az.hotel.model;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "nationality")
@Getter
@Setter
@ToString

public class Nationality {

    @GeneratedValue(generator = "national_seq",strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "national_seq",sequenceName = "seq_national",allocationSize = 1)

    @Id
    private Integer id;

    @Column(name = "nationality")
    private String nationality;

    @Column(name = "nationality_id")
    private Integer nationalityId;

    @OneToMany(mappedBy = "nationality")
    private List<Customer> customers;


}
