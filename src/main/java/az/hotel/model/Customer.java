package az.hotel.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "customers")
@Getter
@Setter
@ToString
public class Customer {
    @GeneratedValue(generator = "customers_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "customers_seq", sequenceName = "seq_customers", allocationSize = 1)
    @Id
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "surname")
    private String surname;
    @Column(name = "dob")
    private Date dob;
    @Column(name = "active")
    private int active;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nationality_id")
    private Nationality nationality;

}
