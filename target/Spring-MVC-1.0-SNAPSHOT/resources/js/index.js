function saveCustomer() {
    var name = $('#name').val();
    var surname = $('#surname').val()
    var dob = $('#dob').val()
    var active = $('#active').val()
    var nationality_id = $('#nationality_id').val()

    $.ajax({
        url: 'services/saveCustomer',
        type: 'post',
        dataType: 'text',
        data: {
            name: name,
            surname: surname,
            dob: dob,
            active: active,
            nationality_id: nationality_id
        },
        success: function () {
            alert("Success")
        }
    })


}