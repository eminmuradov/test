<%--
  Created by IntelliJ IDEA.
  User: Emin-Muradov
  Date: 29-Apr-20
  Time: 23:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
<head>
    <title>Title</title>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/resources/js/index.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#dob" ).datepicker();
        } );
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable();
        });</script>

    <link rel="stylesheet " type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"/>

</head>
<body>
<table id="example" class="display" style="width:100%">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Surname</th>
        <th>Date of Birth</th>
        <th>Active</th>
        <th>Nationality</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="customer" items="${customers}">
        <tr>
            <td>${customer.id}</td>
            <td>${customer.name}</td>
            <td>${customer.surname}</td>
            <td>${customer.dob}</td>
            <td>${customer.active}</td>
            <td>${customer.nationality.nationality}</td>
        </tr>
    </c:forEach>
    </tbody>
    <tfoot>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Surname</th>
        <th>Date of Birth</th>
        <th>Active</th>
        <th>Nationality</th>
    </tr>
    </tfoot>
</table>

<fieldset>
    <form  action="/services/saveCustomer"  method="post">
        <input id="name" name="name" type="text" placeholder="Name"><br>
        <input id="surname" name="surname" type="text" placeholder="surname"><br>
        <input id="dob" name="dob" type="text" placeholder="dob"><br>
        <input id="active" name="active" type="text" placeholder="active"><br>
        <input id="nationality_id" name="nationality_id" type="text" placeholder="nationality"><br>
        <input id="saveBtn" type="submit" onclick="saveCustomer()">
    </form>

</fieldset>

${customer}
${message}

</body>
</html>
